
exports.validString = function (s) {
  if ('string' != typeof s) {
    s = '';
  }
  return s.trim();
}

exports.validEmail = function validEmail (email) {
  var reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  return reg.test(email);
}
