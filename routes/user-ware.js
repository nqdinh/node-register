var passport = require('passport');
var cleanString = require('../helpers/clean-string');
var db = require('../config/dbschema');
var mongoose = require('mongoose');
var url = require('url');

exports.account = function(req, res) {
  res.render('account', { user: req.user });
};

exports.admin = function(req, res) {
  res.send('you do access granted admin!');
};

exports.getLogin = function(req, res) {
  res.render('login', { user: req.user, message: null});
};

// POST /login
//   This is an alternative implementation that uses a custom callback to
//   acheive the same functionality.
exports.postLogin = function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) { return next(err) }
    if (!user) {
      req.session.messages =  [info.message];
      return res.redirect('/login')
    }
    req.logIn(user, function(err) {
      if (err) { return next(err); }
      return res.redirect('/');
    });
  })(req, res, next);
};

exports.logout = function(req, res, next) {
  req.logout();
  res.redirect('/');
};

// forgot password
exports.getForgot = function(req, res) {
  res.render('forgot', {user: req.user, message: null});
};
exports.postForgot = function(req, res) {
  // if input is valid, redirect to login page (with email embedded in url)
  if (cleanString.validEmail(req.body.email)) {
    var emailUri = url.format({
      pathname: 'login',
      search: 'email=' + req.body.email
    });
    res.redirect(emailUri);
  } else {
    // response with error
    res.render('forgot', {user: req.user, message: 'Note: please put a valid email'});
  }
};

// to register new account
exports.getRegister = function(req, res) {
  res.render('signup', { user: req.user, message: 'req.session.messages'});
}

exports.postRegister = function(req, res, next) {
  var username = cleanString.validString(req.body.username);
  var fullname = cleanString.validString(req.body.fullname);
  var email = cleanString.validString(req.body.email);
  var password = cleanString.validString(req.body.password);

  if (!(username && fullname && email && password)) {
    return invalid();
  }
  // user friendly
  email = email.toLowerCase();
  username = username.toLowerCase();

  // by default: admin property is false
  newRegister = {username: req.body.username,
    fullname: req.body.fullname,
    email: req.body.email,
    password: req.body.password};

  addUser(newRegister, function(err) {
    if (err) {
      if (err instanceof mongoose.Error.ValidationError) { return invalid(); } 
      return next(err);
    }
    // else redirect to login with embedded email
    var emailUri = url.format({
      pathname: 'login',
      search: 'email=' + email
    });
    res.redirect(emailUri);
  });

  function invalid () {
    return res.render('signup', {invalid: true });
  }
}

function addUser(user, cb) {
  adm = (user.admin === "true");

  var newUser = new db.userModel(user);
  newUser.save(function(err) {
    if(err) {
      console.log('Error: ', err);
      return cb(err);
    } else {
      console.log('Saved user: ' + user.username);
      return cb(null);
    }
  });

}

