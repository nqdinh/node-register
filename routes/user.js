var pass = require('../config/pass')
var userWare = require('./user-ware');

module.exports = function(app) {
  app.get('/account', pass.ensureAuthenticated, userWare.account);
  app.get('/admin', pass.ensureAuthenticated, pass.ensureAdmin(), userWare.admin);

  app.get('/login', userWare.getLogin);
  app.post('/login', userWare.postLogin);
  app.get('/logout', userWare.logout);

  app.get('/forgot', userWare.getForgot);
  app.post('/forgot', userWare.postForgot);


  app.get('/register', userWare.getRegister);
  app.post('/register', userWare.postRegister);
}
